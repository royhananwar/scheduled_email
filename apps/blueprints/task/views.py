from datetime import datetime, timedelta
import json

from flask import request, Response
from flask_mail import Message

from .models import MailContent, ApplicationSetting
from ...import db, app, mail
from ...utils.helpers import send_scheduled_email
from .import task


@task.route('/save_emails', methods=['POST', ])
def save_emails():
    if request.method == 'POST':

        # define variable
        data = {
            'status': 'success',
            'message': 'Success, the email will be sent automatically'
        }

        # get time utc singapore
        timenow = datetime.utcnow() + timedelta(hours=8)

        # get request json
        event_id = request.json['event_id']
        subject = request.json['email_subject']
        body = request.json['email_body']
        timesend = request.json['timestamp']

        # validation timestamp
        try:
            timesend = datetime.strptime(timesend, '%d %b %Y %H:%M')
            if timesend < timenow:
                data['message'] = 'The parameter timestamp must exceed the current time'
                data['status'] = 'failed'
            else:
                time_difference = timesend - timenow
                second_difference = time_difference.total_seconds()
        except ValueError:
            data['message'] = 'Value Error: use format "%d %b %Y %H:%M" for parameter timestamp'
            data['status'] = 'failed'
        except Exception as e:
            data['message'] = 'Uncaught Error: ' + e.args[0]
            data['status'] = 'failed'

        # validation event_id
        if not isinstance(event_id, int):
            data['message'] = 'parameter event_id must integer'
            data['status'] = 'failed'


        if data['status'] is not 'success':
            response = Response(
                json.dumps(data),
                mimetype='application/json',
                status=400
            )
            return response
        
        # save mail content to db
        mail_content = MailContent()
        mail_content.event_id = event_id
        mail_content.subject = subject
        mail_content.body = body
        mail_content.timesend = timesend

        try:
            db.session.add(mail_content)
            db.session.commit()
        except Exception as e:
            data['message'] = e.args[0]
            data['status'] = 'failed'
        
        if data['status'] is not 'success':
            response = Response(
                json.dumps(data),
                mimetype='application/json',
                status=400
            )
            return response

        # get all recipients
        recipients = ApplicationSetting().get_value_from_key('setting.recipient')
        recipients = recipients.replace(' ', '').split(',')
        
        # create email message
        message = Message(
            subject=subject,
            recipients=recipients,
            body=body
        )
        
        # send email to celery task
        send_scheduled_email.apply_async(args=[message], countdown=int(second_difference))
        
        response = Response(
            json.dumps(data),
                mimetype='application/json',
                status=200
        )

        return response