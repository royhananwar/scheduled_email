from ...import db


class MailContent(db.Model):

    __tablename__ = 'mail_contents'

    id = db.Column(db.Integer, primary_key=True)
    event_id = db.Column(db.Integer)
    subject = db.Column(db.String(255), index=True)
    body = db.Column(db.Text)
    timesend = db.Column(db.DateTime)

    def __repr__(self):
        return 'Subject: {}'.format(self.subject)

class ApplicationSetting(db.Model):

    __tablename__ = 'application_settings'

    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(255), unique=True)
    value = db.Column(db.Text)
    description = db.Column(db.String(255))

    def __repr__(self):
        return '{}'.format(self.value)

    @staticmethod
    def get_value_from_key(key_name):
        result = ApplicationSetting.query.filter(ApplicationSetting.key==key_name).value('value')
        return result