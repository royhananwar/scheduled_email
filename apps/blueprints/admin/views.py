from flask import request, render_template, redirect, url_for, flash

from ..task.models import MailContent, ApplicationSetting
from ...import db
from .import admin


@admin.route('/dashboard', methods=['GET', ])
def dashboard():
    tasks = MailContent.query.order_by(MailContent.timesend.asc()).limit(10).all()
    return render_template('dashboard.html', tasks=tasks)


@admin.route('/tasks', methods=['GET', ])
def tasks():
    tasks = MailContent.query.order_by(MailContent.timesend.asc()).all()
    return render_template('tasks.html', tasks=tasks)


@admin.route('/task/detail/<int:task_id>', methods=['GET', ])
def task_detail(task_id):
    task = MailContent.query.filter(MailContent.id==task_id).first()
    if task:
        return render_template('task_detail.html', task=task)
    else:
        return render_template('404.html')


@admin.route('/application-settings', methods=['GET', ])
def application_settings():
    settings = ApplicationSetting.query.all()
    return render_template('application_settings.html', settings=settings)


@admin.route('/application-settings/update', methods=['POST', ])
def application_settings_update():
    if request.method == 'POST':
        status = 'success'

        # handle recipient
        recipient = request.form.getlist('setting.recipient')
        recipient = ','.join(map(str, recipient))

        setting = ApplicationSetting.query.filter(ApplicationSetting.key=='setting.recipient').first()
        setting.value = recipient
        try:
            db.session.add(setting)
            db.session.commit()
        except Exception:
            status = 'failed'

        # get all keys from request.post
        keys = request.form
        
        for k in keys:
            # skip recipient, already set in
            if k == 'setting.recipient':
                continue
            setting = ApplicationSetting.query.filter(ApplicationSetting.key==k).first()
            if setting:
                setting.value = request.form[k]
                try:
                    db.session.add(setting)
                    db.session.commit()
                except Exception:
                    status = 'failed'
        
        if status == 'success':
            flash('Success update')
        else:
            flash('Something error while updating')
        return redirect(url_for('admin.application_settings'))