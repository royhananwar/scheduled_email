from ..import app, celery, mail


@celery.task(serializer='pickle') # need to use pickle cause will throw error when send email to celery task
def send_scheduled_email(message):
    with app.app_context():
        mail.send(message)