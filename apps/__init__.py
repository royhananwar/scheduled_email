import os

# thrid package
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_mail import Mail
from celery import Celery

# local import
from config import app_config


# define and config flask
app = Flask(__name__)
app.config.from_pyfile('../instance/config.py')

# define database, manager, email
db = SQLAlchemy()
migrate = Migrate()
mail = Mail()

mail.init_app(app)

# define celery and config
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)
app.config.update(accept_content=['json', 'pickle'])


def create_app(config_name):

    app.config.from_object(app_config[config_name])

    # config databse, manager, email
    db.init_app(app)
    migrate.init_app(app, db)

    from .blueprints.task.models import MailContent, ApplicationSetting

    from .blueprints.task import task as blueprint_task
    from .blueprints.admin import admin as blueprint_admin

    app.register_blueprint(blueprint_task)
    app.register_blueprint(blueprint_admin)

    return app