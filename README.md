# Simple application for sending automatic email

## Quick Setup

1. Clone this repository
2. Install redis server. ```sudo apt install redis-server``` for linux and ``` brew install redis``` for mac os.
3. Create virtualenv ```python3 -m venv venv``` and install the requirements.txt
4. Run venv ```source venv/bin/activate```
5. Export flask app ```export FLASK_APP=run.py```
6. Setup database: ```flask db init```, ```flask db migrate```, ```flask db upgrade```, ```flask seeder```
7. Run flask with command ```flask run```
8. Run celery with command ```venv/bin/celery worker -A apps.utils.helpers.celery --loglevel=debug```
9. Go to ```http://localhost:5000/dashboard```


## Feature

1. Add task schedule email. Post data to ```http://localhost:5000/save_emails```.
With payload 
```
{
	"event_id" : 10,
	"email_subject": "test email",
	"email_body": "شاشة خافتة عندما غير نشط",
	"timestamp": "27 Apr 2019 20:04"
}
```
2. List task schedule email. open ```http://localhost:5000/tasks```
3. Setting recipients, open ```http://localhost:5000/application-settings```

.