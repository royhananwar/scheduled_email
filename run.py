import os
import click

from apps import create_app, db
from apps.blueprints.task.models import ApplicationSetting

config_name = os.getenv('FLASK_CONFIG', 'development')
app = create_app(config_name)

SEEDER_APPLICATION_SETTING = [
    {"key": "setting.recipient", "description": "Recipient", "value": "anwarroyhan@gmail.com, roynetflix0519@gmail.com"}
]

@app.cli.command()
def seeder():
    for sas in SEEDER_APPLICATION_SETTING:
        app_setting = ApplicationSetting()
        app_setting.key = sas['key']
        app_setting.description = sas['description']
        app_setting.value = sas['value']
        
        try:
            db.session.add(app_setting)
            db.session.commit()
            click.echo("add key: {}".format(sas['key']))
        except Exception as error:
            click.echo("something error: {}".format(error.args[0]))

if __name__ == "__main__":
    app.run()