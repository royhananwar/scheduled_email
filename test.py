import os
import unittest
import json

from flask import url_for

from instance.config import project_dir
from apps import create_app, db
from run import app
from apps.blueprints.task.models import MailContent

TEST_SAVE_EMAILS_DATA = {
	"event_id" : 10,
	"email_subject": "test email",
	"email_body": "Fix Test",
	"timestamp": "27 Apr 2019 20:04"
}

class TestCase(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(project_dir, 'test.db')
        self.app = app.test_client()
        db.create_all(app=app)
    
    def tearDown(self):
        db.session.remove()
        db.drop_all(app=app)
    
    def test_save_emails(self):
        with app.app_context():
            response = self.app.post(
                url_for('task.save_emails'),
                data=json.dumps(TEST_SAVE_EMAILS_DATA)
            )
            print("finished")
    

if __name__ == "__main__":
    unittest.main()