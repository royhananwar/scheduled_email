import os

project_dir = os.path.dirname(os.path.abspath(__file__))

# Standard config
SECRET_KEY = 'your secret key'
SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(os.path.join(project_dir, '..', "test.db"))


# Celery Config
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_ACCEPT_CONTENT = ['pickle', 'json']

# Flask-Mail Config
MAIL_SERVER ='smtp.gmail.com'
MAIL_PORT = 465
MAIL_USERNAME = 'testanwar99@gmail.com'
MAIL_PASSWORD = '123456.aA'
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_DEFAULT_SENDER = 'test@anwar.com'